package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{
	//feltvariabler
	ArrayList<FridgeItem> fridge = new ArrayList<FridgeItem>();
	int maxSize;
	ArrayList<FridgeItem> expiredItems = new ArrayList<FridgeItem>();

	//Konstruktør
	public Fridge() {
		this.maxSize = 20;
	}

	//Konstruktør2, bruke betingelse for maxSize
	public Fridge(int maxSize) {
		this.maxSize = maxSize;
	}

	@Override
	public int nItemsInFridge() {
		return fridge.size();
	}

	@Override
	public int totalSize() {
		return maxSize;
	}

	@Override
	//sjekker om det er plass i kjøleskapet, om ikke blir det ikke lagt til
	public boolean placeIn(FridgeItem item) {
		if(fridge.size()>= totalSize())
			return false;
		return fridge.add(item);
	}

	@Override
	//!: motsatt boolean
	public void takeOut(FridgeItem item) {
		if(!fridge.contains(item))
			throw new NoSuchElementException("Item not in fridge");
		fridge.remove(item);
	}

	@Override
	//void returner ikke, kun utfører metode
	public void emptyFridge() {
		fridge.clear();
	}

	@Override
	//fjerner utgått mat med laget metode
	public List<FridgeItem> removeExpiredFood() {
		ArrayList<FridgeItem> expiredItems = getExpiredItems();
		fridge.removeAll(expiredItems);
		return expiredItems;
	}
	
	//lager liste over utgåtte items, bruker funksjon fra FridgeItem
	private ArrayList<FridgeItem> getExpiredItems() {
		for(FridgeItem item : fridge) {
			if(item.hasExpired()) {
				expiredItems.add(item);
			}
		}
		return expiredItems;
	}


}
